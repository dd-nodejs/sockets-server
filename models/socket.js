class Socket {
  constructor(io) {
    this.io = io;
    this.socketEvents();
  }

  socketEvents() {
    this.io.on('connection', socket => {
      socket.on('cliente-nuevo-mensaje', data => {
        console.log(data);
        this.io.emit('server-nuevo-mensaje', data);
      });
    });
  }
}

module.exports = Socket;
